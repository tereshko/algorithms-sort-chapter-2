#ifndef STACK_H
#define STACK_H
#include "Array.h"

template< class T>
class Stack {
public:
	Stack();
	void newStack();
	void pushStack();
	void pushValueToStack( T ch);
	T popStack();
	T insertValue();
	void showStack();
	bool isEmpty();

private:
	int m_top;
	Array<T> array;

};

template< class T> Stack<T>::Stack() {
	array.display();
	m_top = -1; // by default, top is -1;
}

template< class T> void Stack<T>::newStack(){
	m_top = -1;
}

template< class T> void Stack<T>::pushStack(){
	T value = insertValue();
	int topValue = ++m_top;
	array.addValueByIndex( topValue, value);
}

template< class T> void Stack<T>::pushValueToStack( T ch){
	int topValue = ++m_top;
	array.addValueByIndex( topValue, ch);
}

template< class T> T Stack<T>::popStack(){
	T topOfStack;
	if( isEmpty()){
		std::cout << "\nStack is empty.";
		pushStack();
	} else {
		topOfStack = array.getValue( m_top);
		m_top--;
	}
	return topOfStack;
}

template< class T> T Stack<T>::insertValue(){
	T value;
	std::cout << "\nPlease, enter the value: ";
	std::cin >> value;
	return value;
}

template< class T> bool Stack<T>::isEmpty(){
	bool isEmpty = false;
	if( m_top == -1) {
		isEmpty = true;
	}
	return isEmpty;
}

#endif