#include "Sorts.h"
#include "Stack.h"
#include "StackCheckCode.h"

#include<iostream>



int main (int argc, char** argv) {

	Sorts sorts;
	Stack<int> stack;
	StackCheckCode checkCode;

	bool isExit = false;
	for (;!isExit;) {
		int selection;
		std::cout << "\n0. Display\n";
		std::cout << "1. Buble sort\n";
		std::cout << "2. Selection sort\n";
		std::cout << "3. Insertion sort\n";
		std::cout << "4. Slip sort\n";
		std::cout << "\nSTACK\n";
		std::cout << "5. New stack\n";
		std::cout << "6. Push to stack\n";
		std::cout << "7. POP stack\n";
		std::cout << "\nSTACK Read Code\n";
		std::cout << "8. Check line with code\n";

		std::cout << "9. Exit\n";
		std::cout << "Please select sort: ";

		std::cin >> selection;


		switch (selection) {
		case 0:
			sorts.display();
			break;
		case 1:
			sorts.bublSort();
			break;
		case 2:
			sorts.selectionSort();
			break;
		case 3:
			sorts.insertionSort();
			break;
		case 4:
			sorts.slipSortStart();
			break;
		case 5:
			stack.newStack();
			break;
		case 6:
			stack.pushStack();
			break;
		case 7:
		{
			bool isEmpty = stack.isEmpty();
			if( isEmpty){
				std::cout << "\n Stack is empty.\n";
			} else {
				int i = stack.popStack();
				std::cout << "POP: " << i << "\n";
			}
			break;
		}
		case 8:
			checkCode.checkCodeLine();
			break;
		case 9:
			isExit = true;
			break;
		default:
			std::cout << "\nIncrorrect selection\n";
		}
	}
}
