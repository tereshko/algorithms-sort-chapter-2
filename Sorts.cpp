#include "Sorts.h"
#include "Array.h"

#include <iostream>

Sorts::Sorts():array() {
	array.addValue(234);
	array.addValue(554);
	array.addValue(2);
	array.addValue(24);
	array.addValue(5);
	array.addValue(15);
}

Sorts::~Sorts(){
}

void Sorts::display(){
	array.display();
}

void Sorts::bublSort() {
	int in, out;
	int nElements = array.getNElements();
	for (out = nElements -1; out > 1; out--) {
		for (in = 0; in< out; in ++) {
			if( array.getValue( in) > array.getValue( in + 1)){
				std::cout << "getValue[" << in << "]: " << array.getValue(in) << ";\n";
				std::cout << "getValue[" << in+1 << "]: " << array.getValue(in+1) << ";\n";
				int temp = array.getValue( in);
				array.addValueByIndex(in, array.getValue( in+1));
				array.addValueByIndex(in+1, temp);
			}
		}
	}
}

void Sorts::selectionSort() {
	int out, in, min;
	for (out = 0; out < array.getNElements()-1; out ++) {
		min = out;
		for (in = out+1; in < array.getNElements(); in++) {
			if( array.getValue(in) < array.getValue(min)){
				min = in;
			}
		}
		int temp = array.getValue( out);
		array.addValueByIndex(out, array.getValue( min));
		array.addValueByIndex(min, temp);
	}
}

void Sorts::insertionSort(){
	int out, in;
	for (out = 1; out < array.getNElements(); out ++) {
		int temp = array.getValue( out);
		in = out;
		while( in > 0 && array.getValue( in - 1) >= temp){
			array.addValueByIndex( in, array.getValue( in - 1));
			--in;
		}
		array.addValueByIndex( in, temp);
	}
}

void Sorts::slipSortStart(){
	int start = 0;
	int end = array.getNElements()-1;
	slipSort( start, end);
}

void Sorts::slipSort( int start, int end){
	int mid = start + ( end - start)/2;
	if (start < end){
		if( start+end == 1) {
			if( array.getValue(start) > array.getValue( end)){
				int temp = array.getValue( start);
				array.addValueByIndex( start, array.getValue( end));
				array.addValueByIndex( end, temp);
			}
		} else {
			slipSort( start, mid);
			slipSort( mid+1 , end);
			merge(start, end);
		}
	}
}

void Sorts::merge( int start, int end){

	int i = start; // nachalo
	int mid = start + (end - start)/2;
	int j = mid +1; //seredina vtoroi polovini
	int k = 0;
	Array<int> priemnyMassyv;

	while ( i<= mid && j <= end) {
		if( array.getValue( i) <= array.getValue( j)){
			priemnyMassyv.addValueByIndex(k, array.getValue( i));
			i++;
		} else {
			priemnyMassyv.addValueByIndex(k, array.getValue( j));
			j++;
		}
		k++;
	}

	while (i <= mid) {
		priemnyMassyv.addValueByIndex( k, array.getValue( i));
		i++;
		k++;
	}
	while ( j <= end) {
		priemnyMassyv.addValueByIndex(k, array.getValue( j));
		j++;
		k++;
	}
	for (i=0; i<k;i++) {
		array.addValueByIndex(start+i, priemnyMassyv.getValue(i));
	}
}




