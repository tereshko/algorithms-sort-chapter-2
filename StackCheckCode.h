#ifndef STACKCHECKCODE_H
#define STACKCHECKCODE_H

#include <string>

class StackCheckCode{
public:
	void input();
	void check();
	void checkCodeLine();


private:
	std::string userLine;
};

#endif