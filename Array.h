#include <iostream>

#ifndef H_ARRAY
#define H_ARRAY

template< class T>
class Array{
public:
	Array();
	~Array();
	void addValue( T value);
	void display();
	T getValue(T index);
	void addValueByIndex( T index, T value);
	int getNElements();

private:
	T* m_array;
	size_t m_arraySize;
	int m_nElement;
};

template< class T> Array<T>::Array(){
	this->m_arraySize = 1000;
	this->m_nElement = 0;
	this->m_array = new T(this->m_arraySize);
}

template< class T> Array<T>::~Array(){
	delete[] this->m_array;
}

template< class T> void Array<T>::addValue( T value){
	this->m_array[this->m_nElement] = value;
	this->m_nElement ++;
}

template< class T> void Array<T>::display() {
	for( int i = 0; i < this->m_nElement; i ++){
		std::cout << this->m_array[i] << "\n";
	}
}

template< class T> T Array<T>::getValue(T index){
	return this->m_array[index];
}

template< class T> void Array<T>::addValueByIndex( T index, T value){
	this->m_array[ index] = value;
	//this->m_nElement++;
}

template< class T>int Array<T>::getNElements(){
	return this->m_nElement;
}
#endif
