#include"Array.h"

#include <iostream>

#ifndef H_SORTS
#define H_SORTS

class Sorts{

public:
	Sorts();
	~Sorts();
	void bublSort();
	void selectionSort();
	void insertionSort();
	void slipSort( int start, int end);
	void slipSortStart();
	void display();

private:
	Array<int> array;
	void merge( int start, int end);
	};

#endif