#include "StackCheckCode.h"
#include "Stack.h"

#include <string>
#include <iostream>



void StackCheckCode::input(){
	std::cout << "\nPlease enter the line with code: ";
	std::cin.ignore();
	std::getline(std::cin, this->userLine);

}

void StackCheckCode::check(){
	int lengthLine = this->userLine.length();
	Stack<char> myStack;
	for (int i = 0; i < lengthLine; i++) {
		char ch = this->userLine[i];
		switch (ch) {
		case '{':
		case '[':
		case '(':
			myStack.pushValueToStack(ch);
			break;
		case '}':
		case ']':
		case ')':
			if( !myStack.isEmpty()){
				char chx = myStack.popStack();
				if( (ch == '}' && chx != '{') ||(ch == ']' && chx != '[') || (ch == ')' && chx != '(')) {
					std::cout << "\n Error: " << ch << " at " << i << "\n";
				} else {
					std::cout << "\n No Errors\n";
				}
			}
			break;
		default:
			break;
		}
	}
	if( myStack.isEmpty()){
		std::cout << "\nStack is empty";
	}
}

void StackCheckCode::checkCodeLine(){
	input();
	check();
}




